INTRODUCTION
------------

This is a simple module to be used together with EU Cookie Compliance and it's
only function is to prevent saving ALL cookies from the installed website if the
user has opted to not allow cookies from the website.

It prevents PHP and Javascript cookies.


FUNCTIONALITY
_____________

When the user clicks on not allow cookies link at EU Cookie Compliance pop up,
this module takes care of blocking all PHP and Javascript temps to save cookies
from all PHP and javascript functions from the current website.

By the obvious reason, the only allowed cookie is the one that says the user has
opted to not save cookies.
